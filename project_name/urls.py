from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from apps.exampleapp import views as exampleapp_views

urlpatterns = [
    url(r'^$', exampleapp_views.main),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
