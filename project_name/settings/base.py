from .shared import *

DEBUG = True
SECRET_KEY = '{{ secret_key }}'

# Allowed hosts if this is !DEBUG
ALLOWED_HOSTS = [
]

# Any environment-specific apps installed here
INSTALLED_APPS += [
]

# You should only have this one your DEBUG environment(s)!
STATICFILES_DIRS = (
  os.path.join(BASE_DIR, 'static/'),
)
