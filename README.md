# Project Template

This is the Django project template I use as a starting point for most of my Django project. Feel free to use it.


## Installing Template

### Create the Virtual Environment

    mkvirtualenv project_name


### Install Django

    pip install django


### Create the project

In your terminal, go to the direct you want to hold your project's files. Don't create the directory yet.

    django-admin startproject --template=https://bitbucket.org/AaronPresley/django-project-template/get/master.zip --extension=py,js,json,less project_name

This will create a new directory named `project_name` on your system.


### Install NPM packages

In order to download the asset files, you'll need to run this:

    npm install

If you've never used `gulp` before, you'll also need to run:

    npm install gulp -g


### Compile Your Assets

    gulp build


### Run Initial Migrations

    python manage.py migrate


### Start The Server

    python manage.py runserver


## Details

Most of my projects have this structure. All JS and Less code goes into the `./frontend` directory
and is compiled down with the `gulp build` command.

Optionally, you can watch for static changes with `gulp watch`.

## Questions?

If you've got questions, you can find me on Twitter [@AaronPresley](https://twitter.com/AaronPresley).
